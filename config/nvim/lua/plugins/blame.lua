return {
  {
    "f-person/git-blame.nvim",
    setup = {
      enabled = true,
    },
    config = function()
      vim.g.gitblame_date_format = "%a, %d %b %Y (%r)"
    end,
  },
}
