# dotfiles

## config

Copy this to your `~/.config` folder

## fonts

Copy this to your `~/.local/share/fonts` folder

## zellij

Copy this to your `~/.config` folder

in `.bashrc` add

`alias mocode='zellij --layout ~/.config/zellij/layouts/dev.kdl options --session-name IDE --default-mode locked'`

## kitty

Copy this to your `~./config/` folder

### Usage for development

`ctrl` + `shift` + `enter` - to split terminal windows
`ctrl` + `shift` + `L` - to move to another layout
`ctrl` to `shift` + `R` - to resize window

`F1` - launch a window pane with `cwd`
`mocode` - starts a dev session in current dir, `nvim .` in main pane

it's not necessary then to use zellij

in `.bashrc` add
`alias mocode='kitty --session dev'`

## alacritty

Copy this to your `~./config/` folder

## .bashrc

```
alias deved='cd ~/development/praca/edtech'
alias devloc='cd ~/development/local'
alias mocode='kitty --session dev'
```
